"use strict";

const fs = require("fs");
let topFolder = "../",
  args = require("minimist")(process.argv.slice(2));

if (args.f != true && args.f != undefined) {
  topFolder += args.f;
} else {
  console.log("\nPlease specify the folder containing artists.\n");
  process.exit(1);
}

function removeExtras(defaultFiles) {
  let files = [];
  for (let file in defaultFiles) {
    let fileName = defaultFiles[file].toLowerCase();
    if (fileName.lastIndexOf(".mp3") != -1 || fileName.lastIndexOf(".ogg") != -1 || fileName.lastIndexOf(".wav") != -1) {
      files.push(defaultFiles[file]);
    }
  }
  return files;
}

function createTracksList(dir) {
  let subdir = dir + "/",
    list = [],
    order,
    files = fs.readdirSync(dir);

  files = removeExtras(files);
  for (let file in files) {
    if (!files.hasOwnProperty(file)) {
      continue;
    }

    let fileName = files[file];
    if (!fs.statSync(subdir + fileName).isDirectory()) {
      list[file] = {};
      list[file].number = Number(file) + 1;
      list[file].name = fileName.slice(0, fileName.lastIndexOf("."));
      list[file].type = fileName.slice(fileName.lastIndexOf("."));
    }
  }
  return list;
}

function createAlbumsList(dir) {
  let subdir = dir + "/",
    list = {},
    order,
    files = fs.readdirSync(dir);

  for (let file in files) {
    if (!files.hasOwnProperty(file)) {
      continue;
    }
    let album = files[file];
    if (fs.statSync(subdir + album).isDirectory()) {
      list[album] = createTracksList(subdir + album);
    }
  }

  if (files.indexOf("order.json") != -1) {
    order = fs.readFileSync(subdir + "order.json");
    order = JSON.parse(order.toString());

    let orderedList = {};
    for (let it in order) {
      orderedList[order[it]] = list[order[it]];
      list[order[it]] = undefined;
    }

    for (let it in list) {
      if (list[it]) {
        orderedList[it] = list[it];
      }
    }
    list = orderedList;
  }
  return list;
}

function createList(dir) {
  let subdir = dir + "/",
    list = {},
    files = fs.readdirSync(dir);

  files.sort(function (a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
  });

  for (let file in files) {
    if (!files.hasOwnProperty(file)) {
      continue;
    }
    let artist = files[file];
    if (fs.statSync(subdir + artist).isDirectory()) {
      list[artist] = createAlbumsList(subdir + artist);
    }
  }
  return list;
}

fs.writeFile("../scripts/musicJson.json", JSON.stringify(createList(topFolder)), function(error) {
    if (error) {
    throw error;
    }
    console.log("Generated musicJson.json");
});
