$(document).ready(function() {
  $(".album").each(function(index, element) {
    Sortable.create(element);
  });

  $("li > span").on("click", function() {
    $(this).parent().remove();
  });

  $("#submit").on("click", function() {
    let list = {};

    $("#artists > li").each(function(index, artist) {
      let artistName = $(artist).attr("data-artist");
      list[artistName] = {};

      $(artist).children().children().each(function(index, album) {
        let albumName = $(album).attr("data-album");
        list[artistName][index] = albumName;
      });
    });

    $.ajax({
      url: "save-order",
      method: "post",
      contentType: "application/json",
      data: JSON.stringify(list),
      success: function() {
        console.log("Order successfully passed to the server.");
      },
      error: function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
      }
    });
  });
});
