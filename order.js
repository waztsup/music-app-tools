#!/usr/bin/env node

"use strict";

const fs = require("fs");

let topFolder = "../",
  albumsCover = false,
  generateJson = false,
  openPage = false,
  args = require("minimist")(process.argv.slice(2));

if (args.f != true && args.f != undefined) {
  topFolder += args.f;
  albumsCover = args.c;
  generateJson = args.g;
  openPage = args.o;
} else {
  console.log("\nPlease specify the folder containing artists.\n");
  process.exit(1);
}

function getImageData(path) {
  if (fs.existsSync(path)) {
    let image = fs.readFileSync(path);
    let imagedata = image.toString("base64");
    return "data:image/jpg;base64," + imagedata;
  } else {
    console.log(path);
    return "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACwAAAAAAQABAAACAkQBADs=";
  }
}

function createAlbumsList(dir) {
  let subdir = dir + "/",
    list = {},
    order,
    files = fs.readdirSync(dir);

  for (let file in files) {
    if (!files.hasOwnProperty(file)) {
      continue;
    }

    let album = files[file];
    if (fs.statSync(subdir + album).isDirectory()) {
      list[album] = [];
    }
  }

  if (files.indexOf("order.json") != -1) {
    order = fs.readFileSync(subdir + "order.json");
    order = JSON.parse(order.toString());

    let orderedList = {};
    for (let it in order) {
      orderedList[order[it]] = list[order[it]];
      list[order[it]] = undefined;
    }

    for (let it in list) {
      if (list[it]) {
        orderedList[it] = list[it];
      }
    }

    list = orderedList;
  }
  return list;
}

function createList(dir) {
  let subdir = dir + "/",
    list = {},
    order = null,
    files = fs.readdirSync(dir);

  files.sort(function (a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
  });

  for (let file in files) {
    if (!files.hasOwnProperty(file)) {
      continue;
    }

    let artist = files[file];
    if (fs.statSync(subdir + artist).isDirectory()) {
      list[artist] = createAlbumsList(subdir + artist);
    }
  }
  return list;
}

function createHtml() {
  let list = createList(topFolder),
    html = '\
    <!DOCTYPE html>\
    <html lang="en">\
      <head>\
        <meta charset="utf-8">\
        <title>Album ordering Tool</title>\
        <link rel="stylesheet" type="text/css" href="index.css"/>\
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>\
        <!--\
        <script src="https://rubaxa.github.io/Sortable/Sortable.js"></script>\
        -->\
        <script src="sortable.js"></script>\
        <script src="index.js"></script>\
      </head>\
      <body>\
        <ul id="artists">\
  ';

  for (let artist in list) {
    html += '\
      <li data-artist="' + artist + '">' + artist + '\
        <ul class="album">\
    ';

    for (let album in list[artist]) {
      html += '\
        <li class="album-name" data-album="' + album + '">\
        ' + (albumsCover ? '<img src="' + getImageData(topFolder + "/" + artist + "/" + album + "/cover.jpg") + '" alt=""/>' : '') + '\
        ' + album + '\
          <span>x</span>\
        </li>\
      ';
    }

    html += '\
        </ul>\
      </li>\
    ';
  }

  html += '\
        </ul>\
        <button id="submit" type="submit">Save order</button>\
      </body>\
    </html>\
  ';

  return html;
}

function handleCssAndJs(req, res) {
  fs.readFile(req.url.replace("/", ""), function (error, data) {
    if (error) {
      console.log(error);
      res.end();
    } else {
      res.writeHead(200, {
        'Content-Type': 'text/' + (req.url.indexOf(".css") ? "css" : "plain") + ''
      });
      res.write(data);
      res.end();
    }
  });
}

function runGenerate(callback) {
  let invoked = false,
    args = ["-f", topFolder.slice(3)],
    process = require("child_process").fork("./generate.js", args);

  process.on("error", function (error) {
    if (invoked) {
      return;
    }
    invoked = true;
    callback(error);
  });

  process.on("exit", function (code) {
    if (invoked) {
      return;
    }
    invoked = true;
    let error = code === 0 ? null : new Error("Exit code: " + code);
    callback(error);
  });
}

function handlePost(req, res) {
  let body = '';
  req.on('data', function (data) {
    body += data;
    if (body.length > 1e6)
      req.connection.destroy();
  });

  req.on('end', function () {
    let order = JSON.parse(body);
    for (let artist in order) {
      fs.writeFile(topFolder + "/" + artist + "/order.json", JSON.stringify(order[artist]), function (error) {
        if (error) {
          console.log(error);
        }
      });
      console.log("Saved album order for: " + artist);
    }

    if (generateJson) {
      runGenerate(function (error) {
        if (error) {
          throw error;
        }
        console.log("Generated musicJson.json");
      });
    }
    res.end();
  });
}

let server = require("http").createServer(function (req, res) {
  if (req.url == "/index.css" || req.url == "/index.js" || req.url == "/sortable.js") {
    handleCssAndJs(req, res);
  } else if (req.method === "POST") {
    handlePost(req, res);
  } else if (req.url == "/") {
    res.writeHead(200, {
      'Content-Type': 'text/html'
    });
    let html = createHtml();
    res.write(html)
    res.end();
  } else {
    console.log("Requested: " + req.url);
    res.end();
  }
});

server.listen(9876, function () {
  console.log("Listening on 9876.");
  if (openPage) {
    const opn = require("opn");
    opn("http://127.0.0.1:9876");
  }
});
