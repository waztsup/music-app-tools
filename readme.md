# music app tools

>Generate the file needed by the music app to work.\
>Create a custom order for artists' albums.


## Install

Clone inside the **music-app** folder.

`$ git clone https://gitlab.com/waztsup/music-app-tools.git`


## Usage

Both will be used inside the **music-app-tools** folder.

`$ node generate -f music-folder`\
and\
`$ node order -f music-folder [options]`

#### options

> `-c`
> Album covers will be displayed before the album name.\
> `-g`
> musicJson will be generated upon saving the order.\
> `-o`
> The app will open automatically in your default browser.


## Folder structure

<details><summary>Required structure</summary>

```
music-app
├── images
│   └── ...
├── music-app-tools
│   └── ...
├── scripts
│   └── ...
├── style
│   └── ...
└── music-folder
    ├── artist-1
    │   └── album
    │       ├── mp3/ogg/wav(s)
    │       └── cover.jpg
    └── artist-2
        ├── album-1
        │   ├── mp3/ogg/wav(s)
        │   └── cover.jpg
        └── album-2
            ├── mp3/ogg/wav(s)
            └── cover.jpg
```
</details>

## Dependencies

### generate

- [minimist](https://www.npmjs.com/package/minimist)

### order

- [minimist](https://www.npmjs.com/package/minimist)
- [opn](https://www.npmjs.com/package/opn)
